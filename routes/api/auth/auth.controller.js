/*
Import & configs
 */
const UserModel = require('../../../models/user.model');
const bcrypt = require('bcryptjs');
//

/*
Functions
 */
// User registration
const register = body => {

    // Search for user
    return new Promise( (resolve, reject) => {

        UserModel.findOne({ email: body.email }, (error, user) => {

            if(error)
                return reject(error);
            else if (user)
                return reject(user);
            else {

                // Hashing password
                bcrypt.hash(body.password, 10)
                    .then( hashedPassword => {
                        body.password = hashedPassword;

                        // Register new User
                        UserModel.create(body, (error, newUser) => {
                            if(error) // Mongo error
                                return reject(error);
                            else // User registrated
                                return resolve(newUser);
                        });

                    })
                    .catch( hashError => {
                        return reject(hashError);
                    });

            }

        });

    });

};

// User login
const login = body => {

    return new Promise( (resolve, reject) => {

        UserModel.findOne( { email: body.email }, (error, user) => {

            if(error)
                reject(error);
            else if(!user)
                reject('User not found');
            else {
                const validPassword = bcrypt.compareSync(body.password, user.password)
                if(!validPassword) {
                    reject('Password not valid');
                } else
                    resolve({
                        user: user,
                        token: user.generateJwt()
                    });
            }

        });

    });

};
//

/*
Exports
 */
module.exports = { register, login };
//