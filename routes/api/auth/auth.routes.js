/*
Imports
 */
// NodeJS
const express = require('express');
const authRouter = express.Router({ mergeParams: true });
// Inner
const { register, login } = require('./auth.controller');
const { checkFields } = require('../../../services/request.checker');
const { sendBodyError, sendFieldsError, sendApiSuccessResponse, sendApiErrorResponse } = require('../../../services/server.response');
//

/*
Routes definition
 */
class AuthRouterClass {

    routes() {

        // HATEOAS
        authRouter.get('/', (req, res) => {
            res.json('HATEOAS for auth');
        });

        // Register
        authRouter.post('/register', (req, res) => {

            // Vérifier la présence du body
            if( typeof req.body === undefined || req.body === null)
                sendBodyError( res, 'No body data provided' );

            // Vérifier les champs du body
            const { ok, extra, miss } = checkFields( ['first_name', 'last_name', 'email', 'password'], req.body );

            // Vérifier si les champs du body sont valides
            if(!ok)
                sendFieldsError( res, 'Bad fields provided', miss, extra );
            else // Register controller function
                register(req.body)
                    .then( apiResponse => sendApiSuccessResponse(res, 'User registrated', apiResponse) )
                    .catch( apiResponse => sendApiErrorResponse(res, 'User registration failed', apiResponse) )
        });

        // Login
        authRouter.post('/login', (req, res) => {

            // Vérifier la présence du body
            if( typeof req.body === undefined || req.body === null)
                sendBodyError( res, 'No body data provided' );

            // Vérifier les champs du body
            const { ok, extra, miss } = checkFields( ['email', 'password'], req.body );

            // Vérifier si les champs du body sont valides
            if(!ok)
                sendFieldsError( res, 'Bad fields provided', miss, extra );
            else // Login controller function
                login(req.body)
                    .then( apiResponse => sendApiSuccessResponse(res, 'User logged in', apiResponse) )
                    .catch( apiResponse => sendApiErrorResponse(res, 'User login failed', apiResponse) )
        });

    };

    init() {
        this.routes();
        return authRouter;
    };

}
//

/*
Export
 */
module.exports = AuthRouterClass;
//