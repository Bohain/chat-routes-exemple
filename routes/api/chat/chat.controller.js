/*
Import & configs
 */
const ChatModel = require('../../../models/chat.model');
//

/*
Functions
 */
// Send a message
const sendMessage = body => {

    return new Promise( (resolve, reject) => {

        ChatModel.create(body, (error, newMessage) => {
            if(error) // Mongo error
                return reject(error);
            else // Message sent
                return resolve(newMessage);
        });

    });

};

// Load all messages
const loadMessagesList = () => {

    return new Promise( (resolve, reject) => {

        ChatModel.find({}, (error, messages) => {

            if(error)
                reject(error);
            else if(!messages)
                reject('No messages to show');
            else {
                resolve(messages);
            }

        })

    });

};

// Delete specific message
const deleteMessage = body => {

    return new Promise( (resolve, reject) => {

        ChatModel.findById(body.id, (error, message) => {

            if(error)
                reject(error);
            else if(!message)
                reject(`There is no message with id ${body.id}`);
            else {

                if(message.user === body.user) {
                    ChatModel.deleteOne(message, (error) => {
                        if(error)
                            reject(error);
                        else
                            resolve('Message removed')
                    })
                } else
                    reject('You\'re not the message\'s owner');

            }

        });

    });

};
//

/*
Exports
 */
module.exports = { sendMessage, loadMessagesList, deleteMessage };
//