/*
Get elements
 */
let ulEl = document.querySelector('ul');
let userInput = document.getElementById('user-input');
let messageInput = document.getElementById('message-input');
let submitButton = document.getElementById('submit-button');
let deleteButtons = document.getElementsByClassName('delete-button');

/*
Méthode pour supprimer un message
TODO: Vérifier l'utilisateur qui veut supprimer le message avec celui qui l'a posté
 */
function deleteMessage(self, id, user) {

    fetch('http://localhost:4000/api/chat/message', {
        method: 'DELETE',
        body: JSON.stringify( { id: id, user: user } ),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(() => {
            ulEl.removeChild(self.parentNode);
        })
        .catch((error) => {
            alert(error);
        });

}

/*
Méthode pour envoyer un message
 */
function sendMessage(message, user) {
    fetch('http://localhost:4000/api/chat/message', {
        method: 'POST',
        body: JSON.stringify( { message: message, user: user } ),
        headers: { 'Content-Type': 'application/json' }
    })
        .then(response => response.json)
        .then(json => {
            let liEl = document.createElement('li');
            let liText = document.createTextNode(user + ' - ' + message);
            let delButton = document.createElement('button');
            let delText = document.createTextNode('Supprimer');
            delButton.classList.add('delete-button');
            delButton.addEventListener('click', function() {
                deleteMessage(this, json._id, user);
            });
            delButton.appendChild(delText);
            liEl.appendChild(liText);
            liEl.appendChild(delButton);
            ulEl.appendChild(liEl);
        })
        .catch((error) => {
            alert(error);
        });
}

for (let i = 0 ; i < deleteButtons.length ; i++) {
    deleteButtons[i].addEventListener('click', function() {
        deleteMessage(this, deleteButtons[i].getAttribute('message-id'), deleteButtons[i].getAttribute('message-user'));
    });
}

submitButton.addEventListener('click', function () {
    sendMessage(messageInput.value, userInput.value);
    messageInput.value = '';
    userInput.value = '';
});