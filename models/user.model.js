/*
Imports & configs
 */
const mongoose = require('mongoose');
const { Schema } = mongoose;
const jwt = require('jsonwebtoken');
//

/*
Model definition
 */
const userSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String
});
//

/*
Methods
 */
userSchema.methods.generateJwt = function generateJwt() {
    // Set expiration
    const expiry = new Date();
    expiry.setDate(expiry.getDate() + 59);
    // JWT Creation
    return jwt.sign({
        _id: this._id,
        email: this.email,
        password: this.password,
        expireIn: '10s', // Temps d'expiration dans la création du token (10sec max pour le créer)
        exp: parseInt(expiry.getTime() / 100, 10)
    }, process.env.JWT_SECRET);
}
//

/*
Exports
 */
const UserModel = mongoose.model('user', userSchema);
module.exports = UserModel;
//